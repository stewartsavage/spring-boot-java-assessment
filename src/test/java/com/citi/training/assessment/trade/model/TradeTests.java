package com.citi.training.assessment.trade.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

import com.citi.training.assessment.trade.model.Trade;

public class TradeTests {

    private int testId = 53;
    private String testStock = "TEST";
    private double testPrice = 100.0;
    private int testVolume = 20;

    @Test
    public void test_Trade_constructor() {
        Trade testTrade = new Trade(testId, testStock,testPrice,testVolume);;

        assertEquals(testId, testTrade.getId());
        assertEquals(testStock, testTrade.getStock());
    }

    @Test
    public void test_Department_toString() {
        String testString = new Trade(testId, testStock,testPrice,testVolume).toString();

        assertTrue(testString.contains((new Integer(testId)).toString()));
        assertTrue(testString.contains(testStock));
    }
}

