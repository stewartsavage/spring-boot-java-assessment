package com.citi.training.assessment.trade.dao;

import com.citi.training.assessment.trade.model.Trade;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static junit.framework.TestCase.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlTradeDaoTests {
    @Autowired
    MysqlTradeDao mysqlTradeDao;

    @Test
    @Transactional
    public void test_deleteById(){
        mysqlTradeDao.create(new Trade(-1,"TEST1",100.0,10));
        mysqlTradeDao.create(new Trade(-1,"TEST2",200.0,20));

        List<Trade> trades = mysqlTradeDao.findAll();
        System.out.println(trades);

        int departmentIDtoDelete = trades.get(0).getId();

        mysqlTradeDao.deleteById(departmentIDtoDelete);

        assertEquals(1, mysqlTradeDao.findAll().size());
    }
    @Test
    @Transactional
    public void test_createAndFindAll(){
        mysqlTradeDao.create(new Trade(-1,"TEST3",300.0,30));

        assertEquals(1, mysqlTradeDao.findAll().size());
    }

    @Test
    @Transactional
    public void test_findById(){
        mysqlTradeDao.create(new Trade(-1,"TEST4",400.0,40));

        Trade tradeToFind = mysqlTradeDao.findAll().get(0);

        int tradeIdToFind = tradeToFind.getId();

        Trade foundTrade = mysqlTradeDao.findById(tradeIdToFind);

        assertEquals(tradeIdToFind,foundTrade.getId());
    }
}
