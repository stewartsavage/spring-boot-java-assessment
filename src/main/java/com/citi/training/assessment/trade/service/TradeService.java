package com.citi.training.assessment.trade.service;

import com.citi.training.assessment.trade.dao.TradeDao;
import com.citi.training.assessment.trade.model.Trade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TradeService {


    @Autowired
    private TradeDao tradeDao;

    public List<Trade> findAll(){
        return tradeDao.findAll();
    }

    public Trade findById(int id){
        return tradeDao.findById(id);
    }

    public Trade create(Trade trade){
        if(trade.getStock().length()>0) {
            return tradeDao.create(trade);
        }
        throw new RuntimeException("invalid parameter: trade stock: "+trade.getStock());
    }

    public void deleteById(int id){
        tradeDao.deleteById(id);
    }
}
