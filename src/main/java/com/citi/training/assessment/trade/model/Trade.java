package com.citi.training.assessment.trade.model;

/**
 * This class represents the trade object, which contains data relating to trades received by the controller and passed
 * through the MysqlTradeDao.
 */

import org.springframework.stereotype.Component;

@Component
public class Trade {

    private int id = 0;
    private String stock = "DFLT";
    private double price = 0.0;
    private int volume = 0;

    public Trade(){}

    public Trade(int id, String stock, double price, int volume){
        this.id = id;
        this.stock = stock;
        this.price = price;
        this.volume = volume;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

   public String toString(){
       return "Trade [id=" + id + ", stock=" + stock +
               ", price=" + price + ", volume="+volume+ "]";
   }

}
