package com.citi.training.assessment.trade.rest;


import java.util.List;

import com.citi.training.assessment.trade.model.Trade;
import com.citi.training.assessment.trade.service.TradeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;


/**
 * REST Interface class for Trade domain object.
 *
 * @author Administrator
 * @see Trade
 * @see <a href="http://www.google/com/">Google</a>
 */


@RestController
@RequestMapping("/trades")
public class TradeController {

    private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);

    @Autowired
    private TradeService tradeService;

    /**
     * Find the list of (@link com.citi.training.assessment.model.Trade)'s
     * @return list of (@link com.citi.training.assessment.model.Trade) that were found or HTTP 404.
     */

    @RequestMapping(method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Trade> findAll(){
        LOG.debug("findAll() was called.");
        return tradeService.findAll();
    }

    /**
     * Find an (@link com.citi.training.assessment.model.Trade) by its integer id.
     * @param id The id of the trade to find
     * @return (@link com.citi.training.assessment.model.Trade) that was found or HTTP 404.
     */

    @RequestMapping(value="/{id}", method=RequestMethod.GET,
            produces=MediaType.APPLICATION_JSON_VALUE)
    public Trade findById(@PathVariable int id) {
        LOG.debug("findById() was called.");
        return tradeService.findById(id);
    }

    /**
     * Create an (@link com.citi.training.assessment.model.Trade) based on JSON data POSTed.
     * @param trade The details of the trade to create
     * @return (@link com.citi.training.assessment.model.Trade) 201 created or other error.
     */

    @RequestMapping(method=RequestMethod.POST,
            consumes=MediaType.APPLICATION_JSON_VALUE,
            produces=MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<Trade> create(@RequestBody Trade trade) {
        LOG.debug("create() was called.");
        return new ResponseEntity<Trade>(tradeService.create(trade),
                HttpStatus.CREATED);
    }

    /**
     * Delete an (@link com.citi.training.assessment.model.Trade) by its integer id.
     * @param id The id of the trade to delete
     * @return (@link com.citi.training.assessment.model.Trade) 204 that was found and deleted or HTTP 404.
     */

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody void deleteById(@PathVariable int id) {
        LOG.debug("deleteById() was called.");
        tradeService.deleteById(id);
    }
}

