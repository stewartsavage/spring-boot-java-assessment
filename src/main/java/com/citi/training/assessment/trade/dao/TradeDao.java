package com.citi.training.assessment.trade.dao;

import com.citi.training.assessment.trade.model.Trade;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface TradeDao {
    List<Trade> findAll();

    Trade findById(int id);

    Trade create(Trade employee);

    void deleteById(int id);

}
