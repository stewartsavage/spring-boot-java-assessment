package com.citi.training.assessment.trade.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@SuppressWarnings("serial")
@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "trade not found")
public class TradeNotFoundException extends RuntimeException {

    public TradeNotFoundException(String msg) {
        super(msg);
    }
}
